#include <string>
#include <algorithm>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include "husky_highlevel_controller/HuskyHighlevelController.hpp"

using namespace std;
using namespace ros;

void scanCallback(const sensor_msgs::LaserScan &msg){

	float min = 1000000;

	for(int i=0; i < sizeof(msg.ranges); i++){
		if(msg.ranges[i] < min){
			min = msg.ranges[i];
		}
	}

	ROS_INFO("Min range: [%f]", min);
}

int main(int argc, char** argv)
{
  init(argc, argv, "husky_highlevel_controller");

  NodeHandle nodeHandle("~");

  int queue_size;
  string topic_name;

  if (!nodeHandle.getParam("topic_name", topic_name)) {
	  ROS_ERROR("Could not find topic_name parameter!");
  }

  if (!nodeHandle.getParam("queue_size", queue_size)) {
	  ROS_ERROR("Could not find queue_size parameter!");
  }

  Subscriber subscriber = nodeHandle.subscribe(topic_name, queue_size, scanCallback);

  husky_highlevel_controller::HuskyHighlevelController huskyHighlevelController(nodeHandle);

  spin();

  return 0;
}
